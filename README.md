Discord bot designed as a fun project.

Has many functions, some of which are completely useless.

I will constantly be adding commands and features as my skills improve.

Feel free to create a pull request for your own features and I'll consider adding them to the repo.

Honourable mentions:

nekoka.tt#4766 - Provided libneko and many hours of programming help

Tmpod#1933 (XLR) - Contributer to libneko and great help to me

LunarCoffee#2553 - Provided inspiration and help with the minesweeper feature