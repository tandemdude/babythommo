# Commands extension made by github u/tandemdude
# https://github.com/tandemdude
from discord.ext import commands
import discord
import psutil
import aiohttp
import random

possible_responses = ['It is certain', 'It is decidely so', 'Without a doubt', 'Yes - definitely',
                      'you may rely on it', 'As I see it, yes', 'Most likely', 'Outlook good',
                      'Yes', 'Signs point to yes', 'Reply hazy, try again', 'Ask again later',
                      'Better not tell you now', 'Cannot predict now', 'Concentrate and ask again',
                      "Don't count on it", 'My reply is no', 'My sources say no', 'Outlook not so good',
                      'Very doubtful']


class CustomCommands(commands.Cog):

    def __init__(self, bot):
        self.bot = bot

    @commands.command(name='8ball')
    async def eightball(self, ctx):
        await ctx.send(f'🎱 {random.choice(possible_responses)}')

    @commands.command()
    async def rps(self, ctx, choice=None):
        rps_options = ['rock', 'paper', 'scissors']
        if choice != 'rock' and choice != 'paper' and choice != 'scissors':
            await ctx.send('You need to specify a choice! Options are: `rock`, `paper` or `scissors`.')
        else:
            bot_choice = random.choice(rps_options)
            if (choice=='rock' and bot_choice=='rock') or (choice=='paper' and bot_choice=='paper') or (choice=='scissors' and bot_choice=='scissors'):
                await ctx.send(f'Draw! We both picked {choice}')
            elif (choice=='rock' and bot_choice=='scissors') or (choice=='paper' and bot_choice=='rock') or (choice=='scissors' and bot_choice=='paper'):
                await ctx.send(f'You win! I picked {bot_choice}')
            elif (choice=='rock' and bot_choice=='paper') or (choice=='paper' and bot_choice=='scissors') or (choice=='scissors' and bot_choice=='rock'):
                await ctx.send(f'I win! My choice was {bot_choice}')
            else:
                await ctx.send(':x: Oh No! You found an error. Please dm my creator, thomm.o#0001, with details. :x:')

    @commands.command()
    async def toss(self, ctx):
        coin_options = ['Heads', 'Tails']
        await ctx.send(f'You flipped a coin. It landed {random.choice(coin_options)}')

    @commands.command()
    async def len(self, ctx, *, arg):
        await ctx.send(f'Your text is `{len(arg)}` characters long!')

    @commands.command()
    async def mock(self, ctx, *, arg):
        await ctx.send(''.join([arg[x].upper() if random.randint(0, 1) else arg[x].lower() for x in range(len(arg))]))

    @commands.command()
    async def info(self, ctx):
        """Views general bot info"""
        embed = discord.Embed(title='Baby-Thommo', colour=0x7289DA)
        embed.set_thumbnail(url=self.bot.user.avatar_url)
        embed.add_field(name='Version:', value='3.0.0 - The currency update', inline=False)
        embed.add_field(name='Library:', value='[discord.py](https://discordpy.readthedocs.io/en/latest/)', inline=False)
        embed.add_field(name='Stats:', value=f'Guilds: {len(self.bot.guilds)}\nUsers: {len([*self.bot.get_all_members()])}', inline=False)
        embed.add_field(name='Source:', value='[Gitlab](https://gitlab.com/tandemdude/babythommo)', inline=False)
        embed.add_field(name='Memory:', value=f'{psutil.virtual_memory().percent}% load')
        embed.set_footer(text='Coded by the one and only thomm.o#0001')
        await ctx.send(embed=embed)

    @commands.command(aliases=['sys', 'sysinfo'])
    async def system(self, ctx):
        """Views general system info"""
        cpu_usage = psutil.cpu_percent(percpu=True)
        ram_usage = psutil.virtual_memory().percent
        swap_usage =psutil.swap_memory().percent
        embed = discord.Embed(title='System Information', description=f'```CPU 0: {cpu_usage[0]}%\nCPU 1: {cpu_usage[1]}%\nCPU 2: {cpu_usage[2]}%\nCPU 3: {cpu_usage[3]}%\n===============\nRAM: {ram_usage}%\nSWAP: {swap_usage}%```', colour=0x7289DA)
        embed.set_thumbnail(url='https://www.raspberrypi.org/wp-content/uploads/2011/10/Raspi-PGB001.png')
        await ctx.send(embed=embed)

    @commands.command()
    async def avatar(self, ctx, user: discord.User=None):
        if user is not None:
            await ctx.send(embed=discord.Embed(title=f"{user.name}'s Avatar:")
                                        .set_image(url=user.avatar_url))
        else:
            await ctx.send(embed=discord.Embed(title=f"{ctx.author.name}'s Avatar:")
                                        .set_image(url=ctx.author.avatar_url))


def setup(bot):
    bot.add_cog(CustomCommands(bot))
