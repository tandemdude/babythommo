from discord.ext import commands
import discord


class Listeners(commands.Cog):
	
	def __init__(self, bot):
		self.bot = bot

	@commands.Cog.listener()
	async def on_message(self, message):
		if self.bot.user in message.mentions:
			await message.channel.send('Hi! My command prefix is `t!`, you can view available commands with `t!help`.')

	@commands.Cog.listener()
	async def on_command_error(self, ctx, error):
		error = error.__cause__ or error
		if isinstance(error, commands.errors.CommandNotFound):
			e = discord.Embed(title="Command Not Found", description="Look at my help menu to see available commands.", colour=0xec6761)
			e.set_footer(text='t!help')
			await ctx.send(embed=e)
		elif isinstance(error, commands.errors.MissingPermissions):
			await ctx.send(embed=discord.Embed(title='Missing Permissions.', description='You don\'t have the right permissions for that command'))
		elif isinstance(error, commands.errors.CommandOnCooldown):
			await ctx.send(embed=discord.Embed(title="Slow it down bud.", description=f"That command is on cooldown.\nYou must wait `{round(error.retry_after)}` seconds before using it again"))

	@commands.Cog.listener()
	async def on_member_join(self, member):
		if member.guild.id == 595480019254640640:
			role_to_add = member.guild.get_role(595487652929601562)
			await member.add_roles(role_to_add, reason="Autorole on join")


def setup(bot):
	bot.add_cog(Listeners(bot))