# Help Command extension coded by github u/tandemdude
# https://github.com/tandemdude
import discord
import json
from discord.ext import commands


class Help(commands.Cog):

    def __init__(self, bot):
        self.bot = bot
        self.prefix = 't!'

    @commands.group(invoke_without_command=True)
    async def help(self, ctx):
        help_embed = discord.Embed(title='Commands Help', colour=0x56f796)
        help_embed.add_field(name=':game_die: Fun', value=f'`{self.prefix}help fun`', inline=True)
        help_embed.add_field(name=':x: Moderation', value=f'`{self.prefix}help moderation`', inline=True)
        help_embed.add_field(name=':keyboard: Extensions', value=f'`{self.prefix}help ext`', inline=True)
        help_embed.set_footer(text=f'{self.prefix}help <category> to view the available commands')
        await ctx.send(embed=help_embed)

    @help.command()
    async def moderation(self, ctx):
        emb = discord.Embed(title='Moderation commands:', description='```purge```', colour=0x56f796)
        await ctx.send(embed=emb)

    @help.command()
    async def fun(self, ctx):
        emb = discord.Embed(title='Misc commands:', description='```8ball, ping, uptime, rps, toss, gay, minesweeper, len, mock```', colour=0x56f796)
        await ctx.send(embed=emb)

    @help.command()
    async def ext(self, ctx):
        emb = discord.Embed(title='Extension manager commands:', description='```listext, loadext, unloadext, reloadext```', colour=0x56f796)
        await ctx.send(embed=emb)


def setup(bot):
    bot.add_cog(Help(bot))
