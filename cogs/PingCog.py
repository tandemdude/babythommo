# Ping extension coded by github u/tandemdude
# https://github.com/tandemdude
import time
import discord
from discord.ext import commands


class Ping(commands.Cog):
	
	def __init__(self, bot):
		self.bot = bot

	@commands.command()
	async def ping(self, ctx):
		start = time.monotonic()
		a = discord.Embed(title=':ping_pong: Ping!', description='Pinging...', colour=0xff0000)
		msg = await ctx.send(embed=a)
		millis = (time.monotonic() - start) * 1000
		heartbeat = ctx.bot.latency * 1000
		b = discord.Embed(title=':ping_pong: Pong!', description=f'Heartbeat: {heartbeat:,.2f}ms\nACK: {millis:,.2f}ms', colour=0xff0000)
		await msg.edit(embed=b)


def setup(bot):
	bot.add_cog(Ping(bot))
