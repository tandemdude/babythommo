import discord
import aiohttp, asyncio
from discord.ext import commands


class MinecraftUtils(commands.Cog):

	def __init__(self, bot):
		self.bot = bot

	@commands.group(invoke_without_command=True, aliases=['minecraft'])
	async def mc(self, ctx):
		embed = discord.Embed(title='Available Subcommands:', colour=0x00ff00)
		embed.add_field(name='skin <username>', value='Get skin for username', inline=True)
		embed.add_field(name='uuid <username>', value='Get UUID for username', inline=True)
		embed.add_field(name='info [ip] [port]', value='Get info for a specific server', inline=True)
		embed.set_footer(text='g!mc <subcommand> <args>')
		await ctx.send(embed=embed)

	@mc.command()
	async def skin(self, ctx, username=None):
		if username == None:
			await ctx.send('You need to specify a username!')
		else:
			url = f'https://minotar.net/armor/body/{username}.png'
			embed = discord.Embed(title=f'{username}\'s Minecraft Skin:', colour=0x00ff00)
			embed.set_image(url=url)
			embed.set_footer(text=f'Requested by {ctx.author}')
			await ctx.send(embed=embed)

	@mc.command()
	async def uuid(self, ctx, username=None):
		if username == None:
			await ctx.send('You need to specify a username!')
		else:
			url = f'https://api.minetools.eu/uuid/{username}'
			async with aiohttp.ClientSession() as session:
				resp = await session.get(url)
				data = await resp.json()
			embed = discord.Embed(title=f'{username}\'s UUID:', description=data['id'], colour=0x00ff00)
			embed.set_author(name=username, icon_url=f'https://minotar.net/helm/{username}.png')
			embed.set_footer(text=f'Requested by {ctx.author}')
			await ctx.send(embed=embed)

	@mc.command()
	async def info(self, ctx, ip: str='patreon.songoda.com', port: int=25565):
		"""
		Gets some info for the server with the specified IP address and port
		Format: <p>players <ip> <port>
		IP defaults to songoda patreon server
		Port defaults to 25565
		"""
		async with aiohttp.ClientSession() as session:
			resp = await session.get(f'https://api.minetools.eu/ping/{ip}/{port}')
			data = await resp.json()
		try:
			embed = discord.Embed(title=f'Info for {ip}', description=f"There are currently {data['players']['online']}/{data['players']['max']} players online", colour=0x00ff00)
			embed.set_footer(text=f"Current server version: {data['version']['name']}")
			await ctx.send(embed=embed)
		except KeyError:
			await ctx.send('Server pinging failed: \nIs it online?\nDid you specify the correct IP/port?')


def setup(bot):
	bot.add_cog(MinecraftUtils(bot))
