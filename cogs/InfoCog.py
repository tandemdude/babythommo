import discord
from discord.ext import commands
import time
import datetime


class Info(commands.Cog):
	
	def __init__(self, bot):
		self.bot = bot

	@commands.command(aliases=["si", "gi", "guildinfo"])
	async def serverinfo(self, ctx):
		guild = ctx.guild
		bots = 0
		for member in guild.members:
			if member.bot:
				bots += 1

		emb = discord.Embed(title="Server Info:", colour=0xffd700)
		emb.set_thumbnail(url=guild.icon_url)
		emb.add_field(name="Name:", value=guild.name, inline=True)
		emb.add_field(name="Owner:", value=str(guild.owner), inline=True)
		emb.add_field(name="Member Count:", value=f"Users: {len(guild.members)-bots}\nBots: {bots}", inline=True)
		emb.add_field(name="Channel Count:", value=f"Text: {len(guild.text_channels)}\nVoice: {len(guild.voice_channels)}", inline=True)
		emb.add_field(name="Role Count:", value=len(guild.roles))
		emb.add_field(name="Region:", value=guild.region, inline=True)
		emb.set_footer(text=f"ID: {guild.id}")
		await ctx.send(embed=emb)

	@commands.command(aliases=["memberinfo"])
	async def mi(self, ctx, member: discord.Member=None):
		member = member if member is not None else ctx.author
		emb = discord.Embed(title=f"Info for user {str(member)}", colour=0xffd700)
		emb.set_thumbnail(url=member.avatar_url)
		emb.add_field(name="Bot:", value=member.bot)
		emb.add_field(name="Status:", value=member.status)
		emb.add_field(name="Nickname:", value=member.display_name)
		emb.add_field(name="Highest Role:", value=member.top_role.name)
		emb.add_field(name="Created:", value=member.created_at.strftime("%Y-%m-%d %H:%M:%S UTC"))
		emb.add_field(name="Joined:", value=member.joined_at.strftime("%Y-%m-%d %H:%M:%S UTC"))
		emb.set_footer(text=f"ID: {member.id}")
		await ctx.send(embed=emb)


def setup(bot):
	bot.add_cog(Info(bot))