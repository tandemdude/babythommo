import discord
from discord.ext import commands


class Suggestions(commands.Cog):
	
	def __init__(self, bot):
		self.bot = bot
		self.owner_id = 215061635574792192

	@commands.command()
	async def suggest(self, ctx, *, suggestion=None):
		owner = self.bot.get_user(self.owner_id)
		if suggestion is None:
			await ctx.send("You need to include something to suggest.")
		elif len(suggestion) <= 2048:
			emb = discord.Embed(title="New Suggestion:", description=suggestion, colour=0x77dd77)
			emb.set_footer(icon_url=ctx.author.avatar_url, text=f"Suggested by {str(ctx.author)}")
			await owner.send(embed=emb)
			await ctx.send("<:greenTick:596576670815879169> Suggestion sent successfully.")
		else:
			await ctx.send("<:redTick:596576672149667840> Your suggestion is too long! It must be less than 2048 characters.")


def setup(bot):
	bot.add_cog(Suggestions(bot))
