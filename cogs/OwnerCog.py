# Bot owner commands extension made by github u/tandemdude
# https://github.com/tandemdude
from discord.ext import commands
import discord
import os
import re


class Owner(commands.Cog):
	def __init__(self, bot):
		self.bot = bot

	@commands.command()
	@commands.is_owner()
	async def kill(self, ctx):
		"""Disconnects the bot from discord and stop the script"""
		await ctx.send('Bot logging out.')
		await self.bot.logout()

	@commands.command()
	@commands.is_owner()
	async def temp(self, ctx):
		"""Gets the current CPU temp of the raspberry pi"""
		temp = os.popen('vcgencmd measure_temp').read()
		temp_stripped = re.findall(r'(\d+(?:\.\d+)?)', temp)
		current_temp = temp_stripped[0]
		await ctx.send(embed=discord.Embed(title=':thermometer: Current operating temperature:', description=f'{current_temp}°C', colour=0xff0000))


def setup(bot):
	bot.add_cog(Owner(bot))