import discord
from discord.ext import commands
import aiosqlite
import aiohttp
import random
import typing
import asyncio


class CurrencyCog(commands.Cog):
	
	def __init__(self, bot):
		self.bot = bot
		self.db_file = "MemberData.db"
		self.table = "users"

	@commands.command(aliases=["join"])
	async def register(self, ctx):
		new_user_cmd = f"INSERT INTO users(user_id, balance, exp) VALUES({ctx.author.id}, 100, 1);"

		async with aiosqlite.connect(self.db_file) as db:
			async with db.execute(f'SELECT * FROM users WHERE(user_id = {ctx.author.id})') as cursor:
				row = await cursor.fetchone()

				if row is not None:
					return await ctx.send("You have already been registered.")

			await db.execute(new_user_cmd)
			await db.commit()
			return await ctx.send("Registration completed ✅\nYou may now use currency commands.")

	@commands.command(aliases=["balance", "wallet"])
	async def bal(self, ctx, user: discord.User=None):
		if user is not None:
			user_id = user.id
		else:
			user_id = ctx.author.id

		async with aiosqlite.connect(self.db_file) as db:
			async with db.execute(f'SELECT * FROM users WHERE(user_id = {user_id})') as cursor:
				row = await cursor.fetchone()

				if row is not None:
					wallet_owner = self.bot.get_user(user_id)
					return await ctx.send(embed=discord.Embed(title=f"<:coin:599350279317487656> {wallet_owner.name}'s balance:",
													   		  description=f"**Wallet:** `{row[1]}` coins",
															  colour=0xffd700))
				else:
					return await ctx.send("No wallet was found.\nYou must register with `t!register` before using currency commands.")

	@commands.cooldown(1, 5, type=commands.BucketType.user)
	@commands.command(aliases=["bet"])
	async def gamble(self, ctx, amount: int=0):
		if amount <= 0:
			return await ctx.send("You need to enter an amount to gamble.")
		else:
			amount_won = 2*amount if random.randint(0,100)<17 else amount*-1
			async with aiosqlite.connect(self.db_file) as db:
				async with db.execute(f"SELECT * FROM users WHERE(user_id = {ctx.author.id});") as cursor:
					row = await cursor.fetchone()

					if row is not None:
						current_bal = row[1]
						if current_bal >= amount:
							new_bal = current_bal + amount_won
							await db.execute(f"UPDATE users SET balance = {new_bal} WHERE user_id = {ctx.author.id}")
							await db.commit()
							if amount_won > 0:
								return await ctx.send(embed=discord.Embed(title="<:coin:599350279317487656> Winner! <:coin:599350279317487656>", description=f"You won `{amount_won}` coins\n\nYour now have `{new_bal}` coins in your wallet", colour=0xffd700)
																			.set_footer(text="Multiplier:  1x"))
							else:
								return await ctx.send(embed=discord.Embed(title="<:coin:599350279317487656> Loser! <:coin:599350279317487656>", description=f"You lost `{abs(amount_won)}` coins\n\nYour now have `{new_bal}` coins in your wallet", colour=0xff6961))
						else:
							return await ctx.send("You do not have enough coins for this action.")
					else:
						return await ctx.send("No wallet was found.\nYou must register with `t!register` before using currency commands.")

	@commands.cooldown(1, 3, type=commands.BucketType.user)
	@commands.command()
	async def slots(self, ctx, amount: int=0):
		"""Not yet implemented"""
		pass

	@commands.cooldown(1, 60, type=commands.BucketType.user)
	@commands.command()
	async def search(self, ctx):

		def weighted_number_generator():
			while True:
				num = random.random()
				weighted_num = round(num * num * 100)
				if weighted_num > 0:
					return weighted_num

		money_found = weighted_number_generator()

		async with aiosqlite.connect(self.db_file) as db:
			async with db.execute(f'SELECT * FROM users WHERE(user_id = {ctx.author.id});') as cursor:
				row = await cursor.fetchone()

				if row is not None:
					current_bal = row[1]
					new_bal = current_bal + money_found
					await db.execute(f"UPDATE users SET balance = {new_bal} WHERE user_id = {ctx.author.id};")
					await db.commit()
					msg = await ctx.send("Searching <a:loading:599345321084190760>")
					await asyncio.sleep(2)
					return await msg.edit(content=f"You searched the area and found `{money_found}` coins which you added to your wallet.")
				else:
					return await ctx.send("Looks like you don't have a wallet.\nYou must register with `t!register` before using currency commands.")

	@commands.cooldown(1, 5, type=commands.BucketType.user)
	@commands.command(aliases=["rich"])
	async def baltop(self, ctx):

		def render_username(user: typing.Union[discord.User, discord.Member], guild: discord.Guild):
			"""Properly handles username rendering for leaderboards"""
			if user in guild.members:
				member = guild.get_member(user.id)
				return member.display_name
			else:
				return str(user)

		async with aiosqlite.connect(self.db_file) as db:
			async with db.execute('SELECT * FROM users ORDER BY balance DESC LIMIT 5') as cursor:
				rows = await cursor.fetchall()
				emb = discord.Embed(title="<:coin:599350279317487656> Balance Leaderboard <:coin:599350279317487656>", colour=0xffd700)
				for i in range(len(rows)):
					user = self.bot.get_user(rows[i][0])
					emb.add_field(name=f"{i+1}) {render_username(user, ctx.guild)}", value=f"Balance: `{rows[i][1]}` coins", inline=False)
				return await ctx.send(embed=emb)

	@commands.cooldown(1, 20, type=commands.BucketType.user)
	@commands.command()
	async def trivia(self, ctx):
		colours = {"easy": 0x77dd77, "medium": 0xffb347, "hard": 0xff6961}
		value = {"easy": 3,"medium": 6,"hard": 9}

		trivia_url = "https://opentdb.com/api.php?amount=1&type=multiple"

		async with aiohttp.ClientSession() as session:
			resp = await session.get(trivia_url)
			data = await resp.json()

		question = data["results"][0]["question"]

		question = question.replace("&quot;", "")
		question = question.replace("&#039;", "'")
		question = question.replace("&rsquo;", "")

		category = data["results"][0]["category"]
		difficulty = data["results"][0]["difficulty"]
		correct_answer = data["results"][0]["correct_answer"]
		incorrect_answers = data["results"][0]["incorrect_answers"]
		incorrect_answers.append(correct_answer)
		all_answers = incorrect_answers

		choices = random.sample(all_answers, len(all_answers))
		prompt = '\n'.join(f'**{i})** {choice}' for i, choice in enumerate(choices, start=1))

		emb = discord.Embed(title=question, description=f"You have 12 seconds to answer\n\n{prompt}", colour=colours[difficulty])
		emb.add_field(name="Difficulty", value=difficulty)
		emb.add_field(name="Value", value=value[difficulty])
		emb.add_field(name="Category", value=category)
		emb.set_author(icon_url=ctx.author.avatar_url, name=f"Trivia question for {ctx.author.name}")
		emb.set_footer(text="You may answer with the number of the answer.")
		e = await ctx.send(embed=emb)

		try:
			msg = await self.bot.wait_for("message", check=lambda m: m.author == ctx.author and m.channel == ctx.channel and m.content.isdigit(), timeout=12)
		except asyncio.TimeoutError:
			emb.description = f"**Out of time.**\n\n{prompt}"
			return await e.edit(embed=emb)

		choice = None
		i = int(msg.content)

		if 0 < i <= len(choices):
			choice = choices[i-1]

		if choice == correct_answer:
			async with aiosqlite.connect(self.db_file) as db:
				async with db.execute(f'SELECT * FROM users WHERE(user_id = {ctx.author.id});') as cursor:
					row = await cursor.fetchone()

					if row is not None:
						current_bal = row[1]
						new_bal = current_bal + value[difficulty]
						await db.execute(f"UPDATE users SET balance = {new_bal} WHERE user_id = {ctx.author.id};")
						await db.commit()
						return await ctx.send(f"Correct! `{value[difficulty]}` coins have been added to your wallet")
					else:
						return await ctx.send(f"Correct! `{value[difficulty]}` coins would have been added to your wallet if you had one.\nrun `t!register` to start earning money.")
		else:
			return await ctx.send(f"Wrong! The correct answer was `{correct_answer}`")


def setup(bot):
	bot.add_cog(CurrencyCog(bot))
